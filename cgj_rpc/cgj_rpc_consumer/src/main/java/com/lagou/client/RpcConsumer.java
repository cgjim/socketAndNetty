package com.lagou.client;

import com.lagou.request.RpcRequest;
import com.lagou.serialize.JSONSerializer;
import com.lagou.utils.RpcDecoder;
import com.lagou.utils.RpcEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RpcConsumer<T> {

    //创建线程池对象
    private static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private static UserClientHandler userClientHandler;

    /**
     * 创建一个代理对象 providerName：UserService#sayHello are you ok?
     * @param serviceClass
     * @param rpcRequest
     * @return
     */
    public Object createProxy(final Class<?> serviceClass,final T rpcRequest){
        //借助JDK动态代理生成代理对象
        return  Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class<?>[]{serviceClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //（1）调用初始化netty客户端的方法

                if(userClientHandler == null){
                    initClient();
                }

                // 设置参数
                //System.out.println("rpcRequest:"+ rpcRequest + ";args[0]:" + args[0]);
                if(rpcRequest instanceof  RpcRequest){
                    ((RpcRequest) rpcRequest).setParameters(args);
                    userClientHandler.setPara(rpcRequest);
                }
                if(rpcRequest instanceof  String){
                    userClientHandler.setPara(rpcRequest.toString() + args[0]);
                    System.out.println("rpcRequest:"+ rpcRequest + ";args[0]:" + args[0]);
                }

                // 去服务端请求数据
                return executor.submit(userClientHandler).get();
            }
        });
    }

    public Object createProxy(final Class<?> serviceClass){
        //借助JDK动态代理生成代理对象
        return  Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class<?>[]{serviceClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //（1）调用初始化netty客户端的方法

                if(userClientHandler == null){
                    initClient();
                }
                //封装
                RpcRequest request = new RpcRequest();
                String requestId = UUID.randomUUID().toString();
                System.out.println(requestId);
                String className = method.getDeclaringClass().getName();
                String methodName = method.getName();
                Class<?>[] parameterTypes = method.getParameterTypes();
                request.setRequestId(requestId);
                request.setClassName(className);
                request.setMethodName(methodName);
                request.setParameterTypes(parameterTypes);
                request.setParameters(args);
                System.out.println("className:"+className);
                System.out.println("methodName:"+methodName);
                System.out.println("parameterTypes:"+parameterTypes);

                // 设置参数
                userClientHandler.setPara(request);
                System.out.println(request);
                System.out.println("设置参数完成");

                // 去服务端请求数据

                return executor.submit(userClientHandler).get();
            }
        });
    }

    /**
     * 2.初始化netty客户端
     * @throws InterruptedException
     */
    public static  void initClient()  {
        userClientHandler = new UserClientHandler();
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY,true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new RpcEncoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(new StringDecoder());
                        pipeline.addLast(userClientHandler);
                    }
                });
        try {
            bootstrap.connect("127.0.0.1",10455).sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
