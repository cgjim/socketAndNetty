package com.lagou.client;

import com.lagou.request.RpcRequest;
import com.lagou.service.UserService;

public class ClientBootStrap {

    public static final String providerName="UserService#sayHello#";

    public static void main(String[] args) throws Exception {
        RpcConsumer rpcConsumer = new RpcConsumer<String>();

            System.out.println("start...");
            UserService proxy = (UserService) rpcConsumer.createProxy(UserService.class);
            while (true){
                Thread.sleep(3000);
                System.out.println("client:");
                System.out.println("client:"+proxy.sayHello("are you ok?"));
            }

    }
}
