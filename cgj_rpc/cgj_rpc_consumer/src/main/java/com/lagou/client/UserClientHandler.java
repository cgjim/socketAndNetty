package com.lagou.client;

import com.lagou.request.RpcRequest;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.Callable;

public class UserClientHandler<T> extends ChannelInboundHandlerAdapter implements Callable{

    private ChannelHandlerContext context;
    private String result;
    private T para;


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception{
        context = ctx;
    }

    /**
     * 收到服务端数据，唤醒等待线程
     */

    @Override
    public synchronized void channelRead(ChannelHandlerContext ctx, Object msg)  throws Exception {
        result = msg.toString();
        notify();
    }

    /**
     * 写出数据，开始等待唤醒
     */

    public synchronized Object call() throws InterruptedException {
        context.writeAndFlush(para);
        wait();
        return result;
    }

    /**
     * 设置参数
     */
    void setPara(T para) {
        this.para = para;
    }



}
