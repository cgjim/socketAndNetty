package com.lagou.service;

import com.lagou.handler.UserServerHandler;
import com.lagou.request.RpcRequest;
import com.lagou.serialize.JSONSerializer;
import com.lagou.utils.RpcDecoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier(value="UserService")
public class UserServiceImpl implements UserService {


    @Override
    public String sayHello(String word) {
        System.out.println("调用成功--参数 "+word + "or not?");
        return "调用成功--参数 "+word  + "or not?";
    }


}
